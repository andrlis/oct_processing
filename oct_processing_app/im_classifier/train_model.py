import torch, torchvision
import torchvision.transforms as T
from torchvision.datasets import ImageFolder
from torch.utils.data import DataLoader

from pathlib import Path

from glob import glob
import shutil
import os
import numpy as np

from datetime import datetime
import argparse

import im_classifier.classifier_constants as cl_const
from im_classifier.utils import *
from im_classifier.oct_model_utils import *
from im_classifier.train_utils import *

RANDOM_SEED = 42
np.random.seed(RANDOM_SEED)
torch.manual_seed(RANDOM_SEED)

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

train_folders = sorted(glob('E:/PyCharm/OCT/OCT2017/OCT2017/train/*'))

DATA_DIR = Path('train_data')

DATASETS = ['train', 'val', 'test']

if __name__ == "__main__":
    """ Prepare data to train model. Split OCT2017 dataset to train, validation and test datasets. """
    parser = argparse.ArgumentParser(description='Train classifier')
    parser.add_argument('-tdata', type=str, help='Path to dataset')
    parser.add_argument('-m', '--model', type=str, default='squeezenet', help='Type of model')

    args = parser.parse_args()

    current_date = datetime.now()
    saved_weights = '{model_name}_{timestamp}'.format(model_name=args.model,
                                                      timestamp=current_date.strftime('%H_%M_%S_%d_%m_%y'))

    train_folders = sorted(glob(args.tdata))

    if not os.path.isdir(os.path.join(os.pardir, DATA_DIR)):
        for ds in DATASETS:
            for cls in cl_const.CLASS_NAMES:
                (DATA_DIR / ds / cls).mkdir(parents=True, exist_ok=True)

        for i, cls_index in enumerate(range(len(cl_const.CLASS_NAMES))):
            image_paths = np.array(glob(f'{train_folders[cls_index]}/*.jpeg'))
            class_name = cl_const.CLASS_NAMES[i]
            print(f'{class_name}: {len(image_paths)}')
            np.random.shuffle(image_paths)

            ds_split = np.split(
                image_paths,
                indices_or_sections=[int(.8 * len(image_paths)), int(.9 * len(image_paths))]
            )

            dataset_data = zip(DATASETS, ds_split)

            for ds, images in dataset_data:
                for img_path in images:
                    shutil.copy(img_path, f'{DATA_DIR}/{ds}/{class_name}/')

    transforms = {'train': T.Compose([
        T.RandomResizedCrop(size=256),
        T.RandomRotation(degrees=15),
        T.RandomHorizontalFlip(),
        T.ToTensor(),
        T.Normalize(cl_const.MEAN_NUMS, cl_const.STD_NUMS)
    ]), 'val': T.Compose([
        T.Resize(size=256),
        T.CenterCrop(size=224),
        T.ToTensor(),
        T.Normalize(cl_const.MEAN_NUMS, cl_const.STD_NUMS)
    ]), 'test': T.Compose([
        T.Resize(size=256),
        T.CenterCrop(size=224),
        T.ToTensor(),
        T.Normalize(cl_const.MEAN_NUMS, cl_const.STD_NUMS)
    ]),
    }

    image_datasets = {
        d: ImageFolder(f'{DATA_DIR}/{d}', transforms[d]) for d in DATASETS
    }

    data_loaders = {
        d: DataLoader(image_datasets[d], batch_size=4, shuffle=True, num_workers=4)
        for d in DATASETS
    }

    dataset_sizes = {d: len(image_datasets[d]) for d in DATASETS}
    cl_const.CLASS_NAMES = image_datasets['train'].classes

    base_model, input_size = initialize_model(args.model, len(cl_const.CLASS_NAMES), True)
    base_model = base_model.to(device)

    base_model, history = train_model(base_model, saved_weights, data_loaders, dataset_sizes, device)

    show_predictions(base_model, cl_const.CLASS_NAMES, data_loaders, n_images=8)
