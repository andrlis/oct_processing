from im_classifier.oct_model_utils import *
import torchvision.transforms as T
import im_classifier.classifier_constants as cl_const


class OCTclassifier:
    def __init__(self, model_name='squeezenet', path_to_weights='', num_classes=4, feature_extract=True,
                 use_pretrained=False):
        self.transforms = {'test': T.Compose([
            T.Resize(size=256),
            T.CenterCrop(size=224),
            T.ToTensor(),
            T.Normalize(cl_const.MEAN_NUMS, cl_const.STD_NUMS)
        ]),
        }

        self.device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
        self.model, _ = initialize_model(model_name, num_classes, feature_extract, use_pretrained)
        self.model.to(self.device)
        self.model.load_state_dict(torch.load(path_to_weights))

    def predict(self, image):
        img = image.convert('RGB')
        img = self.transforms['test'](img).unsqueeze(0)
        pred = self.model(img.to(self.device))
        pred = F.softmax(pred, dim=1)
        pred = pred.detach().cpu().numpy().flatten()

        return np.argmax(pred, axis=0), np.max(pred)
