"""
   Image filtering block
   Created by: Andrei Lisouski (andrlis)
   Filter fabric for image processing
   Available filters:
       * Gaussian filter
       * Median filter
       * Sc filter
       * Histogram equalization
       * Laplacian filter
       * Crimmins filter
"""

from abc import ABC, abstractmethod
from scipy import ndimage, io

import json
import cv2
import numpy as np


#   FILTERING CONSTANTS
FILTER_CONFIG = './config/filters_properties.config'


class AbstractFilter(ABC):

    def __init__(self):
        with open(
                FILTER_CONFIG) as config_file:
            self._properties = json.load(config_file)[self.__class__.__name__]
        super(AbstractFilter, self).__init__()

    @abstractmethod
    def run(self, image):
        pass


class FilterHandler:

    @staticmethod
    def apply_filter(filter: AbstractFilter, image):
        return filter.run(image)


class GaussianFilter(AbstractFilter):

    def __init__(self):
        super().__init__()
        self._kernel = self._properties['kernel']

    def run(self, image):
        return cv2.GaussianBlur(image.copy(), (self._kernel, self._kernel), 0)


class MedianFilter(AbstractFilter):
    def __init__(self):
        super().__init__()
        self._kernel = self._properties['kernel']
        self._iterations = self._properties['iterations']

    def run(self, image):
        image_copy = image.copy()
        for i in range(self._iterations):
            image_copy = cv2.medianBlur(image_copy, self._kernel)
        return image_copy


class ScFilter(AbstractFilter):
    def __init__(self):
        super().__init__()
        self._kernel = self._properties['kernel']
        self._iterations = self._properties['iterations']

    def run(self, image):
        image_copy = image.copy()
        img = image_copy / 255
        for n in range(self._iterations):
            image_copy = np.arctan2(
                ndimage.filters.uniform_filter(np.sin(img), size=self._kernel),
                ndimage.filters.uniform_filter(np.cos(img), size=self._kernel)
            )
        image_copy = image_copy * 255
        return image_copy.astype(np.uint8)


class HistogramEqualizer(AbstractFilter):
    def __init__(self):
        super().__init__()
        self._limit = self._properties['limit']
        self._grid = self._properties['grid']

    def run(self, image):
        image_copy = image.copy()
        gray = cv2.cvtColor(image_copy, cv2.COLOR_BGR2GRAY)
        cliche = cv2.createCLAHE(clipLimit=self._limit, tileGridSize=(self._grid, self._grid))
        return cliche.apply(gray)


class LaplacianFilter(AbstractFilter):
    def __init__(self):
        super().__init__()
        self._kernel = self._properties['kernel']

    def run(self, image):
        img = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        l_img = cv2.Laplacian(img, cv2.CV_16S, self._kernel)
        return cv2.convertScaleAbs(l_img)


class CrimminsFilter(AbstractFilter):

    def run(self, image):
        new_image = image.copy()
        nrow = len(image)
        ncol = len(image[0])

        # Dark pixel adjustment

        # First Step
        # N-S
        for i in range(1, nrow):
            for j in range(ncol):
                if image[i - 1, j, 0] >= image[i, j, 0] + 2:
                    new_image[i, j] += 1
        image = new_image
        # E-W
        for i in range(nrow):
            for j in range(ncol - 1):
                if image[i, j + 1, 0] >= image[i, j, 0] + 2:
                    new_image[i, j] += 1
        image = new_image
        # NW-SE
        for i in range(1, nrow):
            for j in range(1, ncol):
                if image[i - 1, j - 1, 0] >= image[i, j, 0] + 2:
                    new_image[i, j] += 1
        image = new_image
        # NE-SW
        for i in range(1, nrow):
            for j in range(ncol - 1):
                if image[i - 1, j + 1, 0] >= image[i, j, 0] + 2:
                    new_image[i, j] += 1
        image = new_image
        # Second Step
        # N-S
        for i in range(1, nrow - 1):
            for j in range(ncol):
                if (image[i - 1, j, 0] > image[i, j, 0]) and (image[i, j, 0] <= image[i + 1, j, 0]):
                    new_image[i, j] += 1
        image = new_image
        # E-W
        for i in range(nrow):
            for j in range(1, ncol - 1):
                if (image[i, j + 1, 0] > image[i, j, 0]) and (image[i, j, 0] <= image[i, j - 1, 0]):
                    new_image[i, j] += 1
        image = new_image
        # NW-SE
        for i in range(1, nrow - 1):
            for j in range(1, ncol - 1):
                if (image[i - 1, j - 1, 0] > image[i, j, 0]) and (image[i, j, 0] <= image[i + 1, j + 1, 0]):
                    new_image[i, j] += 1
        image = new_image
        # NE-SW
        for i in range(1, nrow - 1):
            for j in range(1, ncol - 1):
                if (image[i - 1, j + 1, 0] > image[i, j, 0]) and (image[i, j, 0] <= image[i + 1, j - 1, 0]):
                    new_image[i, j] += 1
        image = new_image
        # Third Step
        # N-S
        for i in range(1, nrow - 1):
            for j in range(ncol):
                if (image[i + 1, j, 0] > image[i, j, 0]) and (image[i, j, 0] <= image[i - 1, j, 0]):
                    new_image[i, j] += 1
        image = new_image
        # E-W
        for i in range(nrow):
            for j in range(1, ncol - 1):
                if (image[i, j - 1, 0] > image[i, j, 0]) and (image[i, j, 0] <= image[i, j + 1, 0]):
                    new_image[i, j] += 1
        image = new_image
        # NW-SE
        for i in range(1, nrow - 1):
            for j in range(1, ncol - 1):
                if (image[i + 1, j + 1, 0] > image[i, j, 0]) and (image[i, j, 0] <= image[i - 1, j - 1, 0]):
                    new_image[i, j] += 1
        image = new_image
        # NE-SW
        for i in range(1, nrow - 1):
            for j in range(1, ncol - 1):
                if (image[i + 1, j - 1, 0] > image[i, j, 0]) and (image[i, j, 0] <= image[i - 1, j + 1, 0]):
                    new_image[i, j] += 1
        image = new_image
        # Fourth Step
        # N-S
        for i in range(nrow - 1):
            for j in range(ncol):
                if (image[i + 1, j, 0] >= image[i, j, 0] + 2):
                    new_image[i, j] += 1
        image = new_image
        # E-W
        for i in range(nrow):
            for j in range(1, ncol):
                if (image[i, j - 1, 0] >= image[i, j, 0] + 2):
                    new_image[i, j] += 1
        image = new_image
        # NW-SE
        for i in range(nrow - 1):
            for j in range(ncol - 1):
                if (image[i + 1, j + 1, 0] >= image[i, j, 0] + 2):
                    new_image[i, j] += 1
        image = new_image
        # NE-SW
        for i in range(nrow - 1):
            for j in range(1, ncol):
                if (image[i + 1, j - 1, 0] >= image[i, j, 0] + 2):
                    new_image[i, j] += 1
        image = new_image

        # Light pixel adjustment

        # First Step
        # N-S
        for i in range(1, nrow):
            for j in range(ncol):
                if (image[i - 1, j, 0] <= image[i, j, 0] - 2):
                    new_image[i, j] -= 1
        image = new_image
        # E-W
        for i in range(nrow):
            for j in range(ncol - 1):
                if (image[i, j + 1, 0] <= image[i, j, 0] - 2):
                    new_image[i, j] -= 1
        image = new_image
        # NW-SE
        for i in range(1, nrow):
            for j in range(1, ncol):
                if (image[i - 1, j - 1, 0] <= image[i, j, 0] - 2):
                    new_image[i, j] -= 1
        image = new_image
        # NE-SW
        for i in range(1, nrow):
            for j in range(ncol - 1):
                if (image[i - 1, j + 1, 0] <= image[i, j, 0] - 2):
                    new_image[i, j] -= 1
        image = new_image
        # Second Step
        # N-S
        for i in range(1, nrow - 1):
            for j in range(ncol):
                if (image[i - 1, j, 0] < image[i, j, 0]) and (image[i, j, 0] >= image[i + 1, j, 0]):
                    new_image[i, j] -= 1
        image = new_image
        # E-W
        for i in range(nrow):
            for j in range(1, ncol - 1):
                if (image[i, j + 1, 0] < image[i, j, 0]) and (image[i, j, 0] >= image[i, j - 1, 0]):
                    new_image[i, j] -= 1
        image = new_image
        # NW-SE
        for i in range(1, nrow - 1):
            for j in range(1, ncol - 1):
                if (image[i - 1, j - 1, 0] < image[i, j, 0]) and (image[i, j, 0] >= image[i + 1, j + 1, 0]):
                    new_image[i, j] -= 1
        image = new_image
        # NE-SW
        for i in range(1, nrow - 1):
            for j in range(1, ncol - 1):
                if (image[i - 1, j + 1, 0] < image[i, j, 0]) and (image[i, j, 0] >= image[i + 1, j - 1, 0]):
                    new_image[i, j] -= 1
        image = new_image
        # Third Step
        # N-S
        for i in range(1, nrow - 1):
            for j in range(ncol):
                if (image[i + 1, j, 0] < image[i, j, 0]) and (image[i, j, 0] >= image[i - 1, j, 0]):
                    new_image[i, j] -= 1
        image = new_image
        # E-W
        for i in range(nrow):
            for j in range(1, ncol - 1):
                if (image[i, j - 1, 0] < image[i, j, 0]) and (image[i, j, 0] >= image[i, j + 1, 0]):
                    new_image[i, j] -= 1
        image = new_image
        # NW-SE
        for i in range(1, nrow - 1):
            for j in range(1, ncol - 1):
                if (image[i + 1, j + 1, 0] < image[i, j, 0]) and (image[i, j, 0] >= image[i - 1, j - 1, 0]):
                    new_image[i, j] -= 1
        image = new_image
        # NE-SW
        for i in range(1, nrow - 1):
            for j in range(1, ncol - 1):
                if (image[i + 1, j - 1, 0] < image[i, j, 0]) and (image[i, j, 0] >= image[i - 1, j + 1, 0]):
                    new_image[i, j] -= 1
        image = new_image
        # Fourth Step
        # N-S
        for i in range(nrow - 1):
            for j in range(ncol):
                if (image[i + 1, j, 0] <= image[i, j, 0] - 2):
                    new_image[i, j] -= 1
        image = new_image
        # E-W
        for i in range(nrow):
            for j in range(1, ncol):
                if (image[i, j - 1, 0] <= image[i, j, 0] - 2):
                    new_image[i, j] -= 1
        image = new_image
        # NW-SE
        for i in range(nrow - 1):
            for j in range(ncol - 1):
                if (image[i + 1, j + 1, 0] <= image[i, j, 0] - 2):
                    new_image[i, j] -= 1
        image = new_image
        # NE-SW
        for i in range(nrow - 1):
            for j in range(1, ncol):
                if (image[i + 1, j - 1, 0] <= image[i, j, 0] - 2):
                    new_image[i, j] -= 1
        image = new_image
        return new_image.copy()
