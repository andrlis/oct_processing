from PIL import Image, ImageFile


class OctImage:
    def __init__(self):
        self._image = None

    def save_image_to_file(self, filename, extension="JPEG"):
        img = self._image.convert('RGB')

        try:
            extension = 'JPEG' if extension.lower() == 'jpg' else extension.upper()
            img.save("{}.{}".format(filename, extension), format=extension, optimize=True, progressive=True)
        except IOError:
            ImageFile.MAXBLOCK = img.size[0] * img.size[1]
            img.save("{}.{}".format(filename, extension), format=extension, optimize=True, progressive=True)

        def change_colormap(self, cmap):
            pass
