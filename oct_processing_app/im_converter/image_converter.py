# Created by: Andrei Lisouski (@andrlis)
# Build image from OCT data

from PIL import Image, ImageFile
import matplotlib.cm as mpl

import cv2

import numpy as np

color_maps = {
    'AUTUMN': cv2.COLORMAP_AUTUMN,
    'BONE': cv2.COLORMAP_BONE,
    'JET': cv2.COLORMAP_JET,
    'WINTER': cv2.COLORMAP_WINTER,
    'RAINBOW': cv2.COLORMAP_RAINBOW,
    'OCEAN': cv2.COLORMAP_OCEAN,
    'SUMMER': cv2.COLORMAP_SUMMER,
    'SPRING': cv2.COLORMAP_SPRING,
    'COOL': cv2.COLORMAP_COOL,
    'HSV': cv2.COLORMAP_HSV,
    'PINK': cv2.COLORMAP_PINK,
    'HOT': cv2.COLORMAP_HOT,
    'PARULA': cv2.COLORMAP_PARULA}


def array_to_img(img_array, cmap='gray'):
    c_map = mpl.get_cmap(cmap)

    im = c_map(img_array)
    im = np.uint8(im * 255)

    im = Image.fromarray(im)
    return im


def cv2pil(image):
    return Image.fromarray(cv2.cvtColor(image, cv2.COLOR_RGB2BGR))


def pil2cv(image):
    return cv2.cvtColor(np.array(image), cv2.COLOR_RGB2BGR)


def change_color_palette(img, cmap='HOT'):
    cv_image = pil2cv(img)
    cv_image = cv2.applyColorMap(cv_image, color_maps[cmap])
    return cv2pil(cv_image)


def matrix_to_img(img_matrix, cmap):
    pass


def save_img(img, file_name, ext="JPEG"):
    if img.mode in ('RGBA', 'LA'):
        img = img.convert('RGB')
    try:
        ext = 'JPEG' if ext.lower() == 'jpg' else ext.upper()
        img.save("{}.{}".format(file_name, ext), format=ext, optimize=True, progressive=True)
    except IOError:
        ImageFile.MAXBLOCK = img.size[0] * img.size[1]
        img.save("{}.{}".format(file_name, ext), format=ext, optimize=True, progressive=True)
