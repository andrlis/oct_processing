""" Created by: Andrei Lisouski (@andrlis)
    Get information from .mat files """

from scipy import io

#   Mat file attributes constants
MAT_FILE_META_INFO_KEYS = ['__header__', '__version__', '__globals__', '__len__']


class MatFileHandler:
    @staticmethod
    def get_file_data(filename):
        mat_data = io.loadmat(filename)
        data_key = set(mat_data.keys()).difference(MAT_FILE_META_INFO_KEYS)

        if len(data_key) == 1:
            return mat_data[data_key.pop()]
        else:
            raise AssertionError('Invalid source file structure.')
