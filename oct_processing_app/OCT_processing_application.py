"""
Optical coherence tomography image processing application
Created by: Andrei Lisouski (andrlis)
"""

import tkinter as tk
from tkinter import ttk
from tkinter.filedialog import askopenfilename, asksaveasfilename
import tkinter.messagebox as mess_box
from PIL import ImageTk, Image
import os

from im_converter import image_converter
from im_converter import matlab_file_processing
from im_processing.img_preprocessor import *
from im_classifier.classifier import OCTclassifier


class OctApp(tk.Tk):
    def __init__(self):
        super().__init__()

        # APP CONSTANTS
        self.ZOOM_VALUES = ['50%', '75%', '100%', '125%', '150%', '175%', '200%']
        self.COLOR_PALETTES = ['DEFAULT', 'AUTUMN', 'BONE', 'JET', 'WINTER', 'RAINBOW', 'OCEAN', 'SUMMER', 'SPRING',
                               'COOL', 'HSV', 'PINK', 'HOT', 'PARULA']
        self.DISEASE_CLASSES = {0: 'CNV', 1: 'DME', 2: 'DRUSEN', 3: 'NORMAL'}

        # Image
        self.oct_image = None
        self.displayed_img = None
        self.c_map = 'DEFAULT'
        self.zoom = 1

        # Start GUI initialization
        self.title("OCT Processing application")
        self.geometry("1000x500")
        self.minsize(1000, 500)

        # Start configure application menu
        self.main_menu = AppMenu(self)
        self.config(menu=self.main_menu)
        # End configure application menu

        self.left_frame = tk.Frame(self)
        self.img_screen = tk.Canvas(self.left_frame, width=700, height=500, bg="white", scrollregion=(0, 0, 5000, 5000))
        self.hbar = tk.Scrollbar(self.left_frame, orient=tk.HORIZONTAL)
        self.hbar.pack(side=tk.BOTTOM, fill=tk.X)
        self.hbar.config(command=self.img_screen.xview)
        self.vbar = tk.Scrollbar(self.left_frame, orient=tk.VERTICAL)
        self.vbar.pack(side=tk.RIGHT, fill=tk.Y)
        self.vbar.config(command=self.img_screen.yview)

        # self.img_screen = tk.Canvas(self, bg="white", width=700, height=500)
        # self.img_screen.bind("<Configure>", self.resize_canvas)
        self.left_frame.bind("<Configure>", self.resize_canvas)

        self.right_frame = tk.Frame(self, width=300, height=500, bg='gray75')
        self.right_frame.pack_propagate(0)

        # Start configure right panel
        self.t_resize = ToggledFrame(self.right_frame, text='Resize', relief="raised", borderwidth=1)
        self.t_resize.pack(fill="x", expand=0, anchor="n")

        ttk.Label(self.t_resize.sub_frame, text='Zoom:').pack(side="left", fill="x", expand=1)
        self.size_box = ttk.Combobox(self.t_resize.sub_frame,
                                     values=self.ZOOM_VALUES)
        self.size_box.set("100%")
        self.size_box.pack(side="right")
        self.size_box.bind("<<ComboboxSelected>>", self.change_image_size)

        self.t_color = ToggledFrame(self.right_frame, text='Color palette', relief="raised", borderwidth=1)
        self.t_color.pack(fill="x", expand=0, anchor="n")

        ttk.Label(self.t_color.sub_frame, text='Select color palette:').pack(side="left", fill="x", expand=1)
        self.color_box = ttk.Combobox(self.t_color.sub_frame,
                                      values=self.COLOR_PALETTES)
        self.color_box.set("DEFAULT")
        self.color_box.pack(side="right")
        self.color_box.bind("<<ComboboxSelected>>", self.change_color_palette)

        self.t_filter = ToggledFrame(self.right_frame, text='Filters', relief="raised", borderwidth=1)
        self.t_filter.pack(fill="x", expand=0, anchor="n")

        """Start filter panel configuration"""
        self.filter_frame = tk.LabelFrame(self.t_filter.sub_frame, text='Applied filters:')
        self.scrollable_filter_frame = VerticalScrolledFrame(self.filter_frame)

        self.filters = []

        self.filter_control_frame = tk.Frame(self.t_filter.sub_frame)
        self.filter_control_frame.pack(side="top", fill=tk.X)

        self.filters_box = ttk.Combobox(self.filter_control_frame,
                                        values=['Gaussian', 'Median', 'Mean', 'Histogram equalization',
                                                'Laplacian filter', 'Crimmins filter'])
        self.filters_box.pack(side='left', fill=tk.BOTH)

        self.filter_apply_button = tk.Button(self.filter_control_frame, text="Apply")
        self.filter_apply_button.pack(side='left', fill=tk.BOTH)
        self.filter_apply_button.config(command=self.apply_filter)

        self.scrollable_filter_frame.pack(fill="both")
        self.filter_frame.pack(fill="x", expand=0, pady=1, padx=1, side="bottom")
        """End filter panel configuration"""

        self.t_classifier = ToggledFrame(self.right_frame, text='Classifier', relief="raised", borderwidth=1)
        self.t_classifier.pack(fill="x", expand=1, anchor="n")

        self.prediction_lbl = ttk.Label(self.t_classifier.sub_frame, text='Predicted:')
        self.prediction_lbl.pack(side="left", fill="x", expand=1)
        self.get_prediction_btn = ttk.Button(self.t_classifier.sub_frame, text="Get prediction")
        self.get_prediction_btn.config(command=self.classify_image)
        self.get_prediction_btn.pack(side="bottom", anchor="n")

        # End configure right panel

        self.right_frame.pack(side=tk.RIGHT, fill=tk.BOTH)
        self.left_frame.pack(side=tk.LEFT, fill=tk.BOTH, expand=True)
        self.img_screen.pack(side=tk.LEFT, fill=tk.BOTH, expand=True)

        # Lock Control Panel
        self.lock_control_panel()

        # End GUI initialization

    def change_color_palette(self, event=None):
        color_map = self.color_box.get()
        self.c_map = color_map

        width = self.displayed_img.width()
        height = self.displayed_img.height()

        if color_map == 'DEFAULT':
            self.displayed_img = ImageTk.PhotoImage(self.oct_image.img.copy().resize((width, height), Image.BILINEAR))
        else:
            image_with_colormap = image_converter.change_color_palette(self.oct_image.img.copy(), color_map)
            self.displayed_img = ImageTk.PhotoImage(image_with_colormap.resize((width, height), Image.BILINEAR))

        self.reload_canvas()

    def change_image_size(self, event=None):
        zoom = float(self.size_box.get().strip('%')) / 100
        self.zoom = zoom

        self.img_screen.update()
        width = self.img_screen.winfo_width()
        height = self.img_screen.winfo_height()

        if self.c_map == 'DEFAULT':
            self.displayed_img = ImageTk.PhotoImage(
                self.oct_image.img.copy().resize((int(width * zoom), int(height * zoom)), Image.BILINEAR))
        else:
            image_with_colormap = image_converter.change_color_palette(self.oct_image.img.copy(), self.c_map)
            self.displayed_img = ImageTk.PhotoImage(
                image_with_colormap.resize((int(width * zoom), int(height * zoom)), Image.BILINEAR))

        self.reload_canvas()

    def open_image_from_file(self, file_path):
        self.img_screen.update()
        im_width = self.img_screen.winfo_width()
        im_height = self.img_screen.winfo_height()

        file_extension = os.path.splitext(file_path)[1][1:]

        if file_extension.upper() == 'MAT':
            file_data = matlab_file_processing.MatFileHandler.get_file_data(file_path)
            im = image_converter.array_to_img(file_data)
        else:
            im = Image.open(file_path)

        tk_im = ImageTk.PhotoImage(im.resize((im_width, im_height), Image.BILINEAR))
        self.oct_image = OctImage(path=file_path, img=im)
        self.displayed_img = tk_im

        self.unlock_control_panel()
        self.reload_canvas()

    def save_image_to_file(self, new_file_path):
        file_name, file_extension = os.path.splitext(new_file_path)
        image_converter.save_img(self.oct_image.img, file_name, file_extension[1:])
        mess_box.showinfo(message='Файл {} успешно сохранен.'.format(new_file_path))

    def apply_filter(self, event=None):
        filter_name = self.filters_box.get()

        if len(filter_name) > 0:
            new_task = tk.Label(self.scrollable_filter_frame.interior, text=filter_name)
            new_task.pack(side='top')
            self.filters.append(new_task)

        self.img_screen.update()
        im_width = self.img_screen.winfo_width()
        im_height = self.img_screen.winfo_height()

        image = image_converter.pil2cv(self.oct_image.img.copy())

        if filter_name == 'Gaussian':
            self.oct_image.img = image_converter.cv2pil(FilterHandler.apply_filter(GaussianFilter(), image))
        elif filter_name == 'Median':
            self.oct_image.img = image_converter.cv2pil(FilterHandler.apply_filter(MedianFilter(), image))
        elif filter_name == 'Mean':
            self.oct_image.img = image_converter.cv2pil(FilterHandler.apply_filter(ScFilter(), image))
        elif filter_name == 'Histogram equalization':
            self.oct_image.img = image_converter.cv2pil(FilterHandler.apply_filter(HistogramEqualizer(), image))
        elif filter_name == 'Laplacian filter':
            self.oct_image.img = image_converter.cv2pil(FilterHandler.apply_filter(LaplacianFilter(), image))
        elif filter_name == 'Crimmins filter':
            self.oct_image.img = image_converter.cv2pil(FilterHandler.apply_filter(CrimminsFilter(), image))

        if self.c_map == 'DEFAULT':
            self.displayed_img = ImageTk.PhotoImage(
                self.oct_image.img.copy().resize((int(im_width * self.zoom), int(im_height * self.zoom)),
                                                 Image.BILINEAR))
        else:
            image_with_colormap = image_converter.change_color_palette(self.oct_image.img.copy(), self.c_map)
            self.displayed_img = ImageTk.PhotoImage(
                image_with_colormap.resize((int(im_width * self.zoom), int(im_height * self.zoom)), Image.BILINEAR))

        self.reload_canvas()
        self.filters_box.set('')

    def classify_image(self, event=None):
        classifier = OCTclassifier(
            path_to_weights='config/squeezenet_model_state_final.bin',
            feature_extract=True, use_pretrained=True)
        prediction, accuracy = classifier.predict(self.oct_image.img.copy())
        self.prediction_lbl.config(text='Predicted: {pred} ({accur}%)'.format(pred=self.DISEASE_CLASSES.get(prediction),
                                                                              accur=(accuracy * 100)))

    def resize_canvas(self, event):
        try:
            if self.c_map == 'DEFAULT':
                self.displayed_img = ImageTk.PhotoImage(
                    self.oct_image.img.copy().resize((event.width * self.zoom, event.height * self.zoom),
                                                     Image.BILINEAR))
            else:
                image_with_colormap = image_converter.change_color_palette(self.oct_image.img.copy(), self.c_map)
                self.displayed_img = ImageTk.PhotoImage(
                    image_with_colormap.resize((event.width * self.zoom, event.height * self.zoom), Image.BILINEAR))

            self.reload_canvas()
        except Exception:
            pass

    def reload_canvas(self):
        self.img_screen.update()
        im_width = self.img_screen.winfo_width()
        im_height = self.img_screen.winfo_height()

        self.img_screen.configure(scrollregion=(0, 0, int(im_width * self.zoom), int(im_height * self.zoom)))

        self.img_screen.delete("all")
        self.img_screen.create_image(0, 0, image=self.displayed_img, anchor="nw")

    def restore_original(self):
        """Display original image"""
        self.oct_image.img = self.oct_image.original_image

        self.img_screen.update()
        im_width = self.img_screen.winfo_width()
        im_height = self.img_screen.winfo_height()

        self.filters = []

        self.reset_control_panel()

        self.displayed_img = ImageTk.PhotoImage(self.oct_image.img.resize((im_width, im_height), Image.BILINEAR))
        self.reload_canvas()

    def reset_control_panel(self):
        self.size_box.set("100%")
        self.filters_box.set(' ')
        self.color_box.set('DEFAULT')
        self.prediction_lbl.config(text="Predicted:")

        for child in self.filter_frame.winfo_children():
            child.destroy()

    def lock_control_panel(self):
        self.size_box.configure(state=tk.DISABLED)
        self.filter_apply_button.configure(state=tk.DISABLED)
        self.color_box.configure(state=tk.DISABLED)
        self.get_prediction_btn.configure(state=tk.DISABLED)

    def unlock_control_panel(self):
        self.size_box.configure(state=tk.NORMAL)
        self.filter_apply_button.configure(state=tk.NORMAL)
        self.color_box.configure(state=tk.NORMAL)
        self.get_prediction_btn.configure(state=tk.NORMAL)


class OctImage:
    def __init__(self, x=0, y=0, path="", img=None):
        self.x = x
        self.y = y
        self.path = path
        self.img = img
        self.original_image = img.copy()


class ToggledFrame(tk.Frame):

    def __init__(self, parent, text="", *args, **options):
        tk.Frame.__init__(self, parent, *args, **options)

        self.show = tk.IntVar()
        self.show.set(0)

        self.title_frame = ttk.Frame(self)
        self.title_frame.pack(fill="x", expand=1)

        ttk.Label(self.title_frame, text=text).pack(side="left", fill="x", expand=1)

        self.toggle_button = ttk.Checkbutton(self.title_frame, width=2, text='+', command=self.toggle,
                                             variable=self.show, style='Toolbutton')
        self.toggle_button.pack(side="left")

        self.sub_frame = tk.Frame(self, relief="sunken", borderwidth=1)

    def toggle(self):
        if bool(self.show.get()):
            self.sub_frame.pack(fill="x", expand=1)
            self.toggle_button.configure(text='-')
        else:
            self.sub_frame.forget()
            self.toggle_button.configure(text='+')


class VerticalScrolledFrame(tk.Frame):
    def __init__(self, parent, *args, **kw):
        tk.Frame.__init__(self, parent, *args, **kw)

        vscrollbar = tk.Scrollbar(self, orient=tk.VERTICAL)
        vscrollbar.pack(fill=tk.Y, side=tk.RIGHT, expand=tk.FALSE)
        canvas = tk.Canvas(self, bd=0, highlightthickness=0,
                        yscrollcommand=vscrollbar.set)
        canvas.pack(side=tk.LEFT, fill=tk.BOTH, expand=tk.TRUE)
        vscrollbar.config(command=canvas.yview)

        canvas.xview_moveto(0)
        canvas.yview_moveto(0)

        self.interior = interior = tk.Frame(canvas)
        interior_id = canvas.create_window(0, 0, window=interior,
                                           anchor=tk.NW)

        def _configure_interior(event):
            size = (interior.winfo_reqwidth(), interior.winfo_reqheight())
            canvas.config(scrollregion="0 0 %s %s" % size)
            if interior.winfo_reqwidth() != canvas.winfo_width():
                canvas.config(width=interior.winfo_reqwidth())

        interior.bind('<Configure>', _configure_interior)

        def _configure_canvas(event):
            if interior.winfo_reqwidth() != canvas.winfo_width():
                canvas.itemconfigure(interior_id, width=canvas.winfo_width())

        canvas.bind('<Configure>', _configure_canvas)


class AppMenu(tk.Menu):
    def __init__(self, parent: OctApp):
        tk.Menu.__init__(self, parent)
        self.parent = parent

        # Image sub-menu
        self.image_menu = tk.Menu(self, tearoff=0)

        self.open_menu = tk.Menu(self.image_menu, tearoff=0)
        self.open_menu.add_command(label="Из файла", command=self.open_from_file)
        self.open_menu.add_command(label="Из базы данных", state="disabled")

        self.save_menu = tk.Menu(self.image_menu, tearoff=0)
        self.save_menu.add_command(label="Как файл", command=self.save_to_file)
        self.save_menu.add_command(label="Добавить в базу данных", state="disabled")

        self.image_menu.add_cascade(label="Открыть...", menu=self.open_menu)
        self.image_menu.add_cascade(label="Сохранить...", menu=self.save_menu)
        self.image_menu.add_cascade(label="Восстановить оригинал", command=self.restore_image)

        # Application sub-menu
        self.app_menu = tk.Menu(self, tearoff=0)
        self.app_menu.add_command(label="Статус")
        self.app_menu.add_command(label="Параметры")
        self.app_menu.add_command(label="Помощь")

        self.add_cascade(label="Изображение", menu=self.image_menu)
        self.add_cascade(label="Приложение", menu=self.app_menu, state="disabled")

    def open_from_file(self):
        file_name = askopenfilename(
            filetypes=[("JPEG files", "*.jpeg"), ("MATLAB files", "*.mat"), ("PNG files", "*.png"),
                       ("JPG files", "*.jpg")])
        self.parent.open_image_from_file(file_name)

    def save_to_file(self):
        new_file_name = asksaveasfilename(
            filetypes=[("JPEG files", "*.jpeg"), ("PNG files", "*.png"), ("JPG files", "*.jpg")])
        self.parent.save_image_to_file(new_file_name)

    def restore_image(self):
        self.parent.restore_original()


if __name__ == "__main__":
    app = OctApp()
    app.mainloop()
