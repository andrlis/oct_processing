import skimage
import cv2
import time
import os
import numpy as np
import json
import im_processing.img_preprocessor as im_handler
from datetime import datetime

INPUT_DIRECTORY = "/Users/andrlis/Documents/oct_processing/benchmark"
IMAGE_FORMATS = ['.JPEG', '.JPG']


# img = skimage.io.imread(img_path)/255.0

# Add noise to image (gaussian, localvar,  poisson, salt, pepper, s&p, speckle)
def add_noise(img, parameters):
    assert len(parameters) == 1
    mode = parameters[1]

    return skimage.util.random_noise(img, mode=mode)


# mode: 0 - hight; 1 - width
def resize(img, parameters):
    assert len(parameters) == 2

    scale_coef = parameters[0]
    mode = parameters[1]

    r_dim = [img.shape[0], img.shape[1]]

    if mode == 2:
        r_dim[0] = int(img.shape[0] * scale_coef / 100)
        r_dim[1] = int(img.shape[1] * scale_coef / 100)
    else:
        r_dim[mode] = int(img.shape[mode] * scale_coef / 100)

    return cv2.resize(img, tuple(r_dim), interpolation=cv2.INTER_AREA)


def prepare_image(image, function, parameters):
    return function(image, parameters)


def get_images(dir_name):
    list_of_file = os.listdir(dir_name)
    all_files = list()
    for entry in list_of_file:
        full_path = os.path.join(dir_name, entry)
        if os.path.isdir(full_path):
            all_files = all_files + get_images(full_path)
        else:
            _, file_ext = os.path.splitext(full_path)
            if file_ext.upper() in IMAGE_FORMATS:
                all_files.append(full_path)

    return all_files


def signaltonoise(a, axis, ddof=0):
    a = np.asanyarray(a)
    m = a.mean(axis)
    sd = a.std(axis=axis, ddof=ddof)
    return np.where(sd == 0, 0, m / sd)


if __name__ == '__main__':

    handler = im_handler.FilterHandler()

    try:
        source_images = get_images(INPUT_DIRECTORY)
        results = list()

        for image in source_images:
            original_img = cv2.imread(image, 0)
            result = {"image": image}

            # Calculate SNR for original image
            #snr_before_processing = signaltonoise(signaltonoise(original_img, axis=0, ddof=0),axis=0, ddof=0)

            # Process original image
            start_time = time.time()
            img = handler.apply_filter(original_img, im_handler.gaussian_filter)
            end_time = time.time()
            psnr_data = cv2.PSNR(original_img, img)
            #snr_after_processing = signaltonoise(img, axis=0, ddof=0)

            # Save results for original image
            #result.update({"originalSnrDelta": snr_before_processing - snr_after_processing})
            result.update({"originalPSNR": psnr_data})
            result.update({"originalProcessingTime": end_time - start_time})

            distortions = list()

            for i in range(3):
                changed_img = prepare_image(original_img, resize, [40, i])

                # Calculate SNR for resized image
                #snr_before_processing = signaltonoise(changed_img, axis=0, ddof=0)

                # Process resized image
                start_time = time.time()
                img = handler.apply_filter(changed_img, im_handler.gaussian_filter)
                img = handler.apply_filter(img, im_handler.median_filter)
                end_time = time.time()
                psnr_data = cv2.PSNR(changed_img, img)
                #snr_after_processing = signaltonoise(img, axis=0, ddof=0)

                # Save results for resized image
                # distortion = {"snrDelta": snr_before_processing - snr_after_processing,
                #               "PSPR": psnr_data,
                #               "processingTime": end_time - start_time}
                distortion = {"PSNR": psnr_data,
                              "processingTime": end_time - start_time}
                distortions.append(distortion)

            result.update({"distortions": distortions})

            # Save test results
            results.append(result)

        with open('./benchmark_{}.json'.format(datetime.now().strftime("%m.%d.%Y_%H.%M.%S")), 'w') as fout:
            json.dump(results, fout)

    except Exception as ex:
        print("Some errors occurs:\n{}".format(ex))


