import argparse
import cv2
import im_converter.matlab_file_processing as ml
import im_converter.image_converter as converter
import im_processing.img_preprocessor as im_handler
import time
import os

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Matlab files to images module')
    parser.add_argument('indir', type=str, help='Input directory with .mat sources')
    parser.add_argument('outdir', type=str, help='Output directory for images')
    parser.add_argument('-cm', '--cmap', type=str, default='gray', help='Color map for transformed image')

    args = parser.parse_args()

    input_directory = args.indir
    output_directory = args.outdir

    # try:
    #     mat_files = [file for file in os.listdir(input_directory) if
    #                  os.path.isfile(os.path.join(input_directory, file))]
    #
    #     for file in mat_files:
    #         oct_data = ml.get_file_data(os.path.join(input_directory, file))
    #         image = converter.array_to_img(oct_data, args.cmap)
    #
    #         converter.save_img(image, os.path.join(output_directory, os.path.splitext(file)[0]))
    # except Exception as e:
    #     print("OCT data was not converted:\n{}".format(e))

    try:
        result_directory = os.path.join(output_directory, 'proc_result2')
        if not os.path.isdir(result_directory):
            os.mkdir(result_directory)

        oct_images = [file for file in os.listdir(output_directory) if
                      os.path.isfile(os.path.join(output_directory, file))]

        handler = im_handler.FilterHandler()

        start_time = time.time()

        for image in oct_images:
            origin_img = cv2.imread(os.path.join(output_directory, image))
            img = handler.apply_filter(origin_img, im_handler.gaussian_filter)
            img = handler.apply_filter(img, im_handler.sc_filter)

            cv2.imwrite(os.path.join(result_directory, image), img)

        end_time = time.time()
        print('Processing time: {} sec;\nAverage time per image: {} sec'.format(end_time - start_time,
                                                                               (end_time - start_time) / len(
                                                                                   oct_images)))
    except Exception as e:
        print("Some errors occurs during image pre-processing:\n{}".format(e))
